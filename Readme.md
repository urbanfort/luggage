![](https://bytebucket.org/urbanfort/luggage/raw/ab36214517078a4f73aa6b0b7a55361f03221852/luggage.png)

# Luggage
Handy overlapping system-user defaults for NodeJS applications.

## Summary
Luggage provides you with a mechnism in your application to provide a base set of preferences that can be overridden by users.

## Usage

    var luggage = require("luggage")("com.myApp"),
        path = require("path");

    luggage.readFromDefaults("./path/to/defaults.json");

    luggage.persist({
        broker: "file",
        path: [__dirname, "data"].join(path.sep)
    });

    // Recover the header color in the explicit system default.
    myApp.headerColor = luggage.system.read("[com.myApp.]headerColor", "cornflower-blue");

    // Recover the header color user default.
    myApp.headerColor = luggage.default.read("[com.myApp.]headerColor", req.session.UserId, "cornflower-blue");

    // Writing a default (always writes to user space)
    luggage.default.write("[com.myApp.]headerColor", req.session.UserId, "cornflower-blue");

## Events
Rather than using fancy hooks Luggage uses [EventEmitter](http://nodejs.org/api/events.html#events_class_events_eventemitter) to provide extensibility.

### New User Key Created

    luggage.on("luggageIdentityCreated", cb);
    // {who: #}

> When a user key is first created in the memory store.

### Written to Store

    luggage.on("luggageCommitted", cb);
    // {}

> When data in the memory store is persisted.

### Loaded from Store

    luggage.on("luggageRestored", cb);
    // {}

> When data is retrieved from the persistant store.

### Modifications

    luggage.on("luggageValueChanged", cb);
    // {key: #, was: #, now: #, who: #}

> When a user had a value for a given key and it was changed.

### Value Written

    luggage.on("luggageValueWritten", cb);
    // {key: #, now: #, who: #}

> Whenever a value is written for a user.

## Persistence
Implementing persistence for your user defaults is fairly simple. We presently support file storage of user default data. These files are merged at runtime on-demand (using events as your cue.)

    var persistenceThreshold = 30,
        persistenceTicks = 0,
        luggage = require("luggage")();

Tell Luggage that you want to keep data around:

    luggage.persist({broker: "file", path: __dirname});

Bind to the written event and write every once in a while.

    luggage.on(
        "luggageValueWritten",
        function (graph) {
            persistenceTicks++;

            if (persistenceTicks > persistenceThreshold) {
                luggage.persist(); // Will save and then reload.
                luggage.synchronize(false); // Will save only.
                luggage.synchronize(true); // Does the same as persist.
                persistenceTicks = 0;
            }

        }
    );

Or you can write periodically.

    setTimeout(
        luggage.persist,
        3600
    );

Luggage will always try one last time to write its data when the application terminates.
