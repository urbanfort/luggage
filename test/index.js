var nodeunit = require("nodeunit"),
    path = require("path"),
    fs = require("fs"),
    debug = require("debug")("luggage"),
    luggage = require("../")("com.urbanfort.luggage"),
    validUserId = 1,
    otherUserId = 2,
    invalidUserId = 0,
    finalPath = [__dirname, "luggage"].join(path.sep),
    randomwords = require("random-words"),
    lfiles = [];

// luggage.monitor(true);

if (fs.existsSync(finalPath)) {
    lfiles = fs.readdirSync(finalPath);
    _.each(
        lfiles,
        function (file) {
            if (file.indexOf(".json") > -1) {
                debug("rm", ([finalPath, file].join(path.sep)));
                fs.unlinkSync([finalPath, file].join(path.sep));
            }
        }
    );

    debug("rm", finalPath);
    fs.rmdirSync(finalPath);
}

module.exports = {

    setUp:
        function (callback) {
            luggage.readFromDefaults(__dirname + "/system.json");
            callback();
        },

    tearDown:
        function (callback) {
            callback();
        },

    didReadDefaults:
        function (test) {
            test.notEqual(luggage.domainDefaults(), {}, "Luggage is empty.");
            test.done();
        },

    fullUriMatch:
        function (test) {
            test.expect(1);

            test.equal(
                luggage.system.read(
                    "com.urbanfort.luggage.does",
                    "No it don't"
                ),
                "What it do?",
                "Did not find a match for [full uri] specification."
            );
            test.done();
        },

    partialUriMatch:
        function (test) {
            test.expect(1);

            test.equal(
                luggage.system.read(
                    "does",
                    "No it don't"
                ),
                "What it do?",
                "Did not find a match for [partial uri] specification."
            );
            test.done();
        },

    systemKeyOverload:
        function (test) {
            var key = "com.urbanfort.luggage.who";

            test.expect(2);

            luggage.defaults.write(
                key,
                validUserId,
                "Thomas Ingham"
            );

            test.equal(
                luggage.system.read(
                    key,
                    validUserId,
                    "Bob Jones"
                ),
                "Bob Jones",
                "System value was overwritten."
            );


            test.equal(
                luggage.defaults.read(
                    key,
                    validUserId,
                    "Thomas Ingham"
                ),
                "Thomas Ingham",
                "Defaults value was stored incorrectly."
            );


            test.done();
        },

    unknownKeyWrite:
        function (test) {
            var key = "com.urbanfort.luggage.what";

            test.expect(1);

            luggage.defaults.write(
                key,
                validUserId,
                "Something"
            );

            test.equal(
                luggage.defaults.read(
                    key,
                    validUserId,
                    "Nothing"
                ),
                "Something",
                "Uknown key value set incorrectly."
            );
            test.done();
        },

    defaultKeyOverwritten:
        function (test) {
            var key = "com.urbanfort.luggage.when";

            test.expect(2);

            luggage.defaults.write(
                key,
                validUserId,
                "Tomorrow"
            );

            test.equal(
                luggage.defaults.read(
                    key,
                    validUserId,
                    "Today"
                ),
                "Tomorrow",
                "Key set incorrectly the first time."
            );

            luggage.defaults.write(
                key,
                validUserId,
                "Next Tuesday"
            );

            test.equal(
                luggage.defaults.read(
                    key,
                    validUserId,
                    "Tomorrow"
                ),
                "Next Tuesday",
                "Key set incorrectly the second time."
            );

            test.done();
        },

    invalidValueType:
        function (test) {
            var key = "com.urbanfort.luggage.invalid";
            test.expect(3);

            test.throws(
                function () {
                    luggage.defaults.write(key, validUserId, function () {});
                },
                Error,
                "Setting function into a default did not trigger."
            );

            test.throws(
                function () {
                    luggage.defaults.write(key, validUserId, {});
                },
                Error,
                "Setting an object into a default did not trigger."
            );

            test.throws(
                function () {
                    luggage.defaults.write(key, validUserId, [1, 2, 3]);
                },
                Error,
                "Setting an array into a default did not trigger."
            );

            test.done();
        },

    multipleUserDefaultsLength:
        function (test) {
            var key = "com.urbanfort.luggage.me";
            test.expect(1);

            luggage.defaults.write(
                key,
                validUserId,
                "Tom"
            );

            luggage.defaults.write(
                key,
                otherUserId,
                "Bob"
            );

            test.equal(luggage.numberOfDomains(), 2, "Defaults are not the right length");

            test.done();
        },

    objectKey:
        function (test) {
            test.expect(1);
            test.throws(
                function () {
                    luggage.defaults.write({"foo": "bar"}, 1, "Something");
                }
            );
            test.done();
        },

    setupFileStorage:
        function (test) {
            var key = "com.urbanfort.luggage.me";

            luggage.persist({broker: "file", path: __dirname});

            test.expect(3);

            fs.readdir(
                __dirname + "/luggage",
                function (err, files) {
                    _.each(
                        files,
                        function (file) {
                            var data = require(__dirname + "/luggage/" + file);
                            test.ok(_.isEmpty(data[key]) === false, "Could not find key in restored file data.");
                        }
                    );

                    test.equal(
                        luggage.defaults.read(
                            key,
                            1,
                            "Bob"
                        ),
                        "Tom",
                        "Restored value mismatch."
                    );

                    test.done();
                }
            );
        }

};
