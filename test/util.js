var path = require("path"),
    fs = require("fs"),
    debug = require("debug")("luggage"),
    luggage = require("../")("com.urbanfort.luggage"),
    validUserId = 1,
    otherUserId = 2,
    invalidUserId = 0,
    finalPath = [__dirname, "luggage"].join(path.sep),
    randomwords = require("random-words"),
    lfiles = [],
    gnuplot = null;


// luggage.monitor(true);

function removeTestFiles () {
    if (fs.existsSync(finalPath)) {
        lfiles = fs.readdirSync(finalPath);
        _.each(
            lfiles,
            function (file) {
                if (file.indexOf(".json") > -1) {
                    debug("rm", ([finalPath, file].join(path.sep)));
                    fs.unlinkSync([finalPath, file].join(path.sep));
                }
            }
        );

        debug("rm", finalPath);
        fs.rmdirSync(finalPath);
    }
}

function memoryUsageTest () {
    var rows = 74,
        keys = randomwords(rows),
        vals = randomwords(rows),
        count = 0,
        max = 0,
        min = Number.MAX_VALUE,
        data = [],
        output = "",
        graphdat = [[__dirname, "luggage"].join(path.sep), "graph.dat"].join("."),
        filedat = [[__dirname, "luggage"].join(path.sep), "graph.txt"].join("."),
        line = "\n",
        plot;

    luggage.persist({path: __dirname, broker: "file"});

    _.each(
        keys,
        function (key) {
            var ival = parseInt(Math.random() * vals.length),
                mem = (process.memoryUsage().heapUsed / 1024);

            if (mem > max) {
                max = mem;
            }

            if (mem < min) {
                min = mem;
            }

            luggage.defaults.write(
                key,
                validUserId,
                vals[ival]
            );
            count++;
            luggage.persist();
            
            if (gnuplot === null) {
                debug(((process.memoryUsage().heapUsed / 1024) / 1024).toPrecision(2));
            } else {
                data.push([count, mem].join(" "));
            }
        }
    );

    if (gnuplot === null) {
        return;
    }

    debug("Plotting");

    fs.writeFileSync(graphdat, data.join(line), {encoding: "utf8"});

    output = gnuplot()
        .set("term dumb feed 74, 32")
        .set("title 'Memory consumption in Kilobytes over (" + rows + ") keys.'")
        .set("output \"" + filedat + "\"")
        .set("xrange [1:" + rows + "]")
        .set("yrange [" + (min * 0.5) + ":" + (max * 2) + "]")
        .plot("\"" + graphdat + "\"")
        .end();

    if (fs.existsSync(filedat)) {
        setTimeout(
            function () {
                console.log(fs.readFileSync(filedat, {encoding: "utf8"}));
            },
            1000
        );
    } else {
        debug("Filedat missing", filedat);
    }

    debug("Plot Complete!");
}

if (require.main === module) {
    gnuplot = require("gnuplot");
    removeTestFiles();
    memoryUsageTest();
} else {

    if (require.main.filename.indexOf("/nodeunit") > -1) {
        console.log("\n", "You can also sample memory usage with the command:", "\n\t", "$:node test/util");
    } else {
        module.exports.removeTestFiles = removeTestFiles;
        module.exports.memoryUsageTest = memoryUsageTest;
    }

}
