// # Luggage
// Your friendly defaults storage bags.
var fs = require("fs"),
    path = require("path"),
    _ = require("underscore"),
    merge = require("merge"),
    events = require("events"),
    debug = require("debug")("luggage"),
    luggage = {

        // ## NS
        // You can set a hint for all future queries that will automatically
        // be prepended to each fetch/set.
        ns: null,

        // ## Persisted
        // Use your database or other system to maintain preferences between
        // application launches.
        // Use:
        //     luggage.persist(databaseConfiguration);
        persisted: false,

        persistanceIdentity: "UserId",

        // ## Synchronize
        // Stubbed to prevent exceptions from early calls.
        synchronize:
            function () {
            },

        // ## Persist
        // Use this method to provide a database broker for keeping state of
        // the defaults over time.
        persist:
            function (params) {
                var finalPath;

                if (_.isEmpty(params)) {
                    luggage.synchronize(true);
                    return;
                }

                if (params.hasOwnProperty("broker") === false) {
                    throw new Error("You asked to persist defaults without a broker.");
                }

                switch (params.broker) {
                    case "mysql": // {host: "", user: "", password: "", port: ""}
                        // set up mysql driver
                        // assign db reference
                        break;
                    case "file": // {path: "", fileperuser: true|false}
                        // create writeable folder at location
                        if (_.has(params, "path") === false) {
                            throw new Error("You asked to use file storage but didn't provide a path. :/");
                        }

                        finalPath = [params.path, "luggage"].join(path.sep);

                        if (fs.existsSync(params.path) === false) {
                            throw new Error("You asked to use file storage @" + params.path + " but it doesn't exist.");
                        }
                        
                        if (fs.existsSync(finalPath) === false) {
                            fs.mkdirSync(finalPath);
                        }

                        if (fs.existsSync(finalPath) === false) {
                            throw new Error("I couldn't create the luggage folder @" + finalPath);
                        }

                        // Set a boolean to tell when we've set up.
                        luggage.persisted = true;

                        luggage.synchronize = function (willReload) {
                            var ident,
                                didWrite = false,
                                reload = _.isEmpty(willReload) ? false : willReload;

                            for (ident in luggage._defaults) {
                                fs.writeFileSync(
                                    [finalPath, [ident, "json"].join(".")].join(path.sep),
                                    JSON.stringify(luggage._defaults[ident], null, 4),
                                    {encoding: "utf8"}
                                );
                                didWrite = true;
                            }

                            if (didWrite) {
                                luggage.emit(
                                    "luggageCommitted",
                                    {}
                                );
                            }

                            if (reload === false) {
                                return;
                            }

                            fs.readdir(
                                finalPath, 
                                function (err, files) {
                                    var match = /.*\.json$/,
                                        didRead = false;
                                    if (err) {
                                        throw err;
                                    }

                                    _.each(
                                        files,
                                        function (file) {
                                            if (match.test(file)) {
                                                var data = require([finalPath, file].join(path.sep));
                                                luggage._defaults[ident] = data;
                                                didRead = true;
                                            }
                                        }
                                    );

                                    if (didRead) {
                                        luggage.emit(
                                            "luggageRestored",
                                            {}
                                        );
                                    }
                                }
                            );

                        };

                        break;
                }

                luggage.synchronize(true);
            },

        expand:
            function (key) {
                if (_.isEmpty(luggage.ns)) {
                    return key;
                }

                if (key.indexOf(luggage.ns) > -1) {
                    return key;
                }

                return [luggage.ns, key].join(".");
            },

        system: {
            read:
                function (key, defaultValue) {
                    key = luggage.expand(key);

                    if (luggage._system.hasOwnProperty(key) &&
                        _.isEmpty(luggage._system[key]) === false) {
                        return luggage._system[key];
                    } else {
                        return defaultValue;
                    }
                }
        },

        defaults: {
            read:
                function (key, ident, defaultValue) {
                    var union = luggage.union(ident);

                    key = luggage.expand(key);

                    if (union.hasOwnProperty(key) &&
                        _.isEmpty(union[key]) === false) {

                        return union[key];

                    } else {

                        return defaultValue;

                    }

                },

            write:
                function (key, ident, setValue) {

                    if ((key instanceof Object)) {
                        throw new Error("You cannot create a key out of anything other than a string.");
                    }

                    key = luggage.expand(key);

                    if (setValue instanceof Object) {
                        throw new Error("You may only set integral types in defaults.");
                    }

                    if (_.isEmpty(setValue)) {
                        throw new Error("You may not set defaults to nothing. Consider using [false|'none'|0] instead.");
                    }

                    if (luggage._defaults.hasOwnProperty(ident) === false) {

                        luggage._defaults[ident] = {};

                        luggage.emit(
                            "luggageIdentityCreated",
                            {
                                who: ident
                            }
                        );

                    }

                    if (luggage._defaults[ident].hasOwnProperty(key)) {

                        luggage.emit(
                            "luggageValueChanged",
                            {
                                key: key,
                                was: luggage.defaults.read(key, ident),
                                now: setValue
                            }
                        );

                    } else {

                        luggage.emit(
                            "luggageValueWritten",
                            {
                                key: key,
                                now: setValue
                            }
                        );

                    }

                    luggage._defaults[ident][key] = setValue;
                }

        },

        union:
            function (ident) {
                var result = merge(
                    true,
                    luggage._system,
                    _.has(luggage._defaults, ident) ? luggage._defaults[ident] : {}
                );

                return result;
            },

        readFromDefaults:
            function (path, callback) {
                if (fs.existsSync(path)) {
                    this._system = require(path);
                } else {
                    debug("No defaults at ", path);
                }

                if (_.isEmpty(callback) === false) {
                    debug("readFromDefaults callback");
                    callback();
                }
            },

        _system:
            {
            },

        _defaults:
            {
            },

        numberOfDomains:
            function () {
                var obj,
                    total = 0;
                for (obj in luggage._defaults) {
                    total++;
                }
                return total;
            },

        setNameSpace:
            function (ns) {
                "use strict";
                this.ns = ns;
            },

        domainDefaults:
            function () {
                return this._system;
            },

        logDomainDefaults:
            function (log) {
                var _debug;

                if (_.isEmpty(log)) {
                    _debug = debug;
                } else {
                    _debug = log;
                }

                _debug(luggage.domainDefaults());
            },

        emitter: new events.EventEmitter(),

        emit:
            function (e, params) {
                params.evt = e;
                luggage.emitter.emit(e, params);
            },

        on:
            function (e, cb) {
                luggage.emitter.on(e, cb);
            },

        monitor:
            function (log) {
                if (log === null || log === false) {
                    luggage.emitter
                        .removeListener(
                            "luggageIdentityCreated",
                            luggage.out
                        )
                        .removeListener(
                            "luggageCommitted",
                            luggage.out
                        )
                        .removeListener(
                            "luggageRestored",
                            luggage.out
                        )
                        .removeListener(
                            "luggageValueChanged",
                            luggage.out
                        )
                        .removeListener(
                            "luggageValueWritten",
                            luggage.out
                        );
                    debug("No longer listening for changes.");
                } else {
                    luggage.on(
                        "luggageIdentityCreated",
                        luggage.out
                    );
                    luggage.on(
                        "luggageCommitted",
                        luggage.out
                    );
                    luggage.on(
                        "luggageRestored",
                        luggage.out
                    );
                    luggage.on(
                        "luggageValueChanged",
                        luggage.out
                    );
                    luggage.on(
                        "luggageValueWritten",
                        luggage.out
                    );
                    debug("Listening for changes.");
                }
            },

        out:
            function () {
                debug(arguments);
            },

        shutdown:
            function () {
                debug("Shutting down Luggage. Have a nice trip!");
                if (luggage.persisted) {
                    luggage.synchronize(false);
                }
            }
    };

module.exports = luggage;

process.on(
    "exit",
    luggage.shutdown
);

process.on(
    "SIGINT",
    luggage.shutdown
);
