var luggage = require("./lib/luggage"),
    debug = require("debug")("luggage");
    _ = require("underscore");

module.exports = function (optionalNamespace) {

    if (_.isEmpty(optionalNamespace) === false) {
        luggage.setNameSpace(optionalNamespace);
    }

    return luggage;

};
